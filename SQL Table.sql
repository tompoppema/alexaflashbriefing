USE [MAATWERK]
GO

/****** Object:  Table [dbo].[AlexaEchoTexts]    Script Date: 04/02/2019 15:01:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[AlexaEchoTexts](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[titleText] [varchar](max) NULL,
	[mainText] [varchar](max) NULL,
	[FileName] [varchar](100) NULL,
	[sysCreated] [datetime] NOT NULL,
 CONSTRAINT [PK_AlexaEchoTexts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[AlexaEchoTexts] ADD  CONSTRAINT [DF_AlexaEchoTexts_sysCreated]  DEFAULT (getdate()) FOR [sysCreated]
GO

