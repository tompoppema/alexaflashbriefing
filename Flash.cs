﻿namespace AlexaFlash
{
    internal class Flash
    {
        public string uid { get; set; }
        public string updateDate { get; set; }
        public string titleText { get; set; }
        public string mainText { get; set; }
    }
}