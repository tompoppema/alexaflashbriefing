﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace AlexaFlash
{
    class Program
    {
        public static void Main()
        {
            string storageConnectionString = ConfigurationManager.AppSettings["storageConnectionString"];
            //Console.WriteLine(storageConnectionString);
            ProcessAsync().GetAwaiter().GetResult();

        }

        public static async Task ProcessAsync()
        {
            CloudStorageAccount storageAccount = null;
            CloudBlobContainer cloudBlobContainer = null;
            string sourceFile = null;
            string localFileName="";
            string ser="";
            string json = "";
            var obj = new Flash();
            string storageConnectionString = ConfigurationManager.AppSettings["storageConnectionString"]; 

            // Check whether the connection string can be parsed.
            if (CloudStorageAccount.TryParse(storageConnectionString, out storageAccount))
            {
                try
                {
                    // Create the CloudBlobClient that represents the Blob storage endpoint for the storage account.
                    CloudBlobClient cloudBlobClient = storageAccount.CreateCloudBlobClient();

                    cloudBlobContainer = cloudBlobClient.GetContainerReference("alexa"); // cloudBlobClient.GetContainerReference("quickstartblobs" + Guid.NewGuid().ToString());

                    // Create a file in your local MyDocuments folder to upload to a blob.
                    string localPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                    

                    SqlCommand cmd = new SqlCommand("select id, mainText, titleText, [FileName] from AlexaEchoTexts (nolock) where maintext<>''");
                    cmd.CommandType = CommandType.Text;
                    DataTable dt = GetData(cmd);
                    if (dt.Rows.Count != 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            obj = new Flash
                            {
                                uid = "ID " + dr["id"].ToString(),
                                updateDate = DateTime.UtcNow.ToString("s") + "Z",
                                titleText = dr["titleText"].ToString(),
                                mainText = dr["mainText"].ToString()
                            };
                            ser = JsonConvert.SerializeObject(obj, Formatting.Indented);
                            localFileName = dr["FileName"].ToString();
                            sourceFile = Path.Combine(localPath, localFileName);
                            File.WriteAllText(sourceFile, ser);

                            Console.WriteLine("Temp file = {0}", sourceFile);
                            Console.WriteLine("Uploading to Blob storage as blob '{0}'", localFileName);
                            Console.WriteLine();

                            // Get a reference to the blob address, then upload the file to the blob.
                            // Use the value of localFileName for the blob name.
                            CloudBlockBlob cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(localFileName);
                            await cloudBlockBlob.UploadFromFileAsync(sourceFile);
                        }
                        
                    }
                   
                }
                catch (StorageException ex)
                {
                    Console.WriteLine("Error returned from the service: {0}", ex.Message);
                }
            }
            else
            {
                Console.WriteLine(
                    "A connection string has not been defined in the system environment variables. " +
                    "Add a environment variable named 'storageconnectionstring' with your storage " +
                    "connection string as a value.");
            }
        }
        public static DataTable GetData(SqlCommand cmd)
        {
            //Algemene functie om data op te halen. Met "gridview.datasource = dt" kan vervolgens een gridview worden gevuld.
            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["connMaatwerk"].ConnectionString);
            SqlDataAdapter sda = new SqlDataAdapter();
            cmd.Connection = con;
            cmd.CommandTimeout = 120;
            con.Open();
            sda.SelectCommand = cmd;
            sda.Fill(dt);
            con.Close();
            return dt;
        }

    }
}
